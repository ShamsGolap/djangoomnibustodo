/*globals angular, console, prompt, alert, WebSocket, Omnibus */

(function (ng) {
    'use strict';

    var // Attributes
        TodoControllers = ng.module('TodoControllers', []);

    TodoControllers.controller('TodoCtrl', ['$scope', function ($scope) {
        var // Function
            addTodo,
            toggleTodoDone,
            editTodo,
            deleteTodo,
            refreshList,

            // Attributes
            editTodoNewLabel;

        addTodo = function () {
            if ($scope.newTodo) {
                $scope.newTodo = '';
            }
        };

        toggleTodoDone = function (todo) {
            todo.done = !todo.done;
        };

        editTodo = function (todo) {
            editTodoNewLabel = prompt('Please enter the new label for the todo: ', todo.label);
            todo.label = editTodoNewLabel;
            if (editTodoNewLabel) {

            }
        };

        deleteTodo = function (todo) {

        };

        refreshList = function () {

        };

        $scope.todos = [];
        $scope.newTodo = '';

        $scope.addTodo = addTodo;
        $scope.toggleTodoDone = toggleTodoDone;
        $scope.editTodo = editTodo;
        $scope.deleteTodo = deleteTodo;
        $scope.refreshList = refreshList;
    }]);

}(angular));
