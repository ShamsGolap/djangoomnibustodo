/*global angular */

(function (ng) {
    'use strict';

    ng.module('TodoApp', [
        'TodoControllers'
    ]);
}(angular));
