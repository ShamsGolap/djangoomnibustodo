from django.shortcuts import render
from django.http import HttpResponse

from app.models import Todo

class IndexView():
    template_name = 'index.html'
    context_object_name = 'todo_list'

    def get_queryset(self):
        return Todo.objects.all()
